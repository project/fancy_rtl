  <div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
	<h2 class="node-title">
	<?php if ($page == 0): ?>
    <a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title; ?></a>
	 <?php else : ?> <?php print $title; ?> <?php endif; ?></h2>
	<?php if ($submitted): ?>
    <span class="submitted"><?php print $submitted?></span>
    <span class="taxonomy"><?php print $terms?></span><?php endif; ?>
    <div class="content"><?php print $content?></div>
    <br class="clear" />
  <?php if ($links) { ?><div class="links">&raquo; <?php print $links?></div><?php }; ?>
  </div>