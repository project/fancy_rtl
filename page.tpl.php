<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language; ?>" xml:lang="<?php print $language; ?>">
<head>
<title><?php print $head_title ?></title>
<?php print $head ?><?php print $styles ?>
</head>
<body<?php print $onload_attributes ?>>
<table border="0" cellpadding="0" cellspacing="0" id="header">
  <tr>
    <td rowspan="2" id="logo"> <?php if ($logo) { ?>      <a href="<?php print $base_path ?>" title="Home"><img src="<?php print $logo ?>" alt="Home" /></a>      <?php } ?></td>
  </tr>
  <tr>
  <td id="fname">
      <?php if ($site_name) { ?><h1 class='site-name'><a href="<?php print $base_path ?>" title="Home"><?php print $site_name ?></a></h1><?php } ?>
      <?php if ($site_slogan) { ?><div class='site-slogan'><?php print $site_slogan ?></div><?php } ?>    </td>
    
    <td id="menu">
<?php print $search_box ?><?php if (isset($primary_links)) { ?><div id="primary"><?php print theme('links', $primary_links) ?></div><?php } ?>
	</td>
  </tr>
     <tr>
    <td colspan="3"><div><?php print $header ?></div></td>
  </tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" id="content">
  <tr>
    <?php if ($sidebar_right) { ?><td id="sidebar-right">
      <?php print $sidebar_right ?>
    </td><?php } ?>
    <td valign="top">
      <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
      <div id="main">
		 <?php if ($node == 0): ?>
        <h1 class="title"><?php print $title ?></h1>   <?php endif; ?>
        <div class="tabs"><?php print $tabs ?></div>
        <?php print $help ?>
        <?php print $messages ?>

        <?php print $content; ?>
      </div>
    </td>
    <?php if ($sidebar_left) { ?><td id="sidebar-left">
      <?php print $sidebar_left ?>
    </td><?php } ?>
  </tr>
</table>

<table id="footer" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <th id="copyleft" scope="col">Copyleft &copy; <?php print $site_name ?>
	<p><?php print $footer_message ?></p>
    </th>
  </tr>
</table>

<?php print $closure ?>
</body>
</html>